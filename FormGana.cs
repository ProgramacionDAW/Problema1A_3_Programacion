﻿using System;
using System.Windows.Forms;
using System.Media;

namespace Problema1
{
    public partial class FormGana : Form
    {
        SoundPlayer soundGana = new SoundPlayer(Properties.Resources.sonidoGana);
        public FormGana()
        {
            InitializeComponent();
            soundGana.Play();
        }

        private void FormGana_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
