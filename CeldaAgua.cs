﻿namespace Problema1
{
    class CeldaAgua:Celda
    {
        #region Constructor
        public CeldaAgua()
        {

        }

        public CeldaAgua(int fila, int columna):base(fila,columna)
        {

        }
        #endregion

        #region Metodos
        public override string Mostrar(Form1 formulario, int jugador)
        {
            estado = Estados.Agua;
            return Estado;
        }
        #endregion
    }
}
