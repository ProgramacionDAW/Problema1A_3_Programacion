﻿using System;
using System.Windows.Forms;
using System.Media;

namespace Problema1
{
    public partial class FormPierde : Form
    {
        SoundPlayer soundPierde = new SoundPlayer(Properties.Resources.sonidoPierde);
        public FormPierde()
        {
            InitializeComponent();
            soundPierde.Play();
        }

        private void FormPierde_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
