﻿namespace Problema1
{
    class Juego
    {
        #region Atributos
        int puntuacion = 100;
        Tablero tablero;
        #endregion

        #region Constructor
        public Juego()
        {
            tablero = new Tablero();
        }
        #endregion

        #region Propiedades
        public Tablero TableroJuego
        {
            get
            {
                return tablero;
            }
        }

        public int Puntuacion
        {
            get
            {
                return puntuacion;
            }
            set
            {
                puntuacion = value;
            }
        }
        #endregion
    }
}
