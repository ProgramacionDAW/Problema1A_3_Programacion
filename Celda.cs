﻿namespace Problema1
{
    abstract class Celda
    {
        #region Atributos
        protected enum Estados
        {
            Tocado,
            Hundido,
            Agua,
        }
        protected Estados estado;
        protected int fila;
        protected int columna;
        bool activo;
        protected int id;
        #endregion

        #region Constructor
        public Celda()
        {
            activo = true;
        }

        public Celda(int fila, int columna)
        {
            this.fila = fila;
            this.columna = columna;
            activo = true;
        }

        public Celda(int fila, int columna, int id)
        {
            this.fila = fila;
            this.columna = columna;
            this.id = id;
            activo = true;
        }
        #endregion

        #region Metodos
        public abstract string Mostrar(Form1 formulario, int jugador);
        #endregion

        #region Propiedades
        public int Fila
        {
            get
            {
                return fila;
            }
        }

        public int Columna
        {
            get
            {
                return columna;
            }
        }

        public int ID
        {
            get
            {
                return id;
            }
        }

        public bool Activo
        {
            get
            {
                return activo;
            }
            set
            {
                activo = value;
            }
        }

        public string Estado
        {
            get
            {
                return estado.ToString();
            }
        }
        #endregion
    }
}
