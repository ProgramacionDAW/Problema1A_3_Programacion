﻿using System;
using System.Windows.Forms;

namespace Problema1
{
    static class Program
    {
        /// <summary>
        /// Punto de entrada principal para la aplicación.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
        static public Juego juego = new Juego();
        static public Juego juego2;
        static public int jugadores = 1;
        static public Datos datos = new Datos();
    }
}
