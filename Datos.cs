﻿using System.Collections.Generic;
using System.Linq;
using System.Data.SQLite;
using System.Data;
using System.IO;

namespace Problema1
{
    class Datos
    {
        #region Atributos
        List<Jugador> datos;
        SQLiteConnection conexion;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor base
        /// </summary>
        public Datos()
        {
            if(File.Exists("FAME.hall"))
            {
                conexion = new SQLiteConnection("Data Source=FAME.hall;Version=3;");
            }
            else
            {
                CrearBaseDatos();
            }
            datos = new List<Jugador>(11); //ponemos 11 como máximo ya que vamos a agregar un jugador de más
            LeerDatos();
        }
        #endregion

        #region Metodos
        /// <summary>
        /// Crea una nueva base de datos en caso de que no exista
        /// </summary>
        private void CrearBaseDatos()
        {
            SQLiteConnection.CreateFile("FAME.hall");
            conexion = new SQLiteConnection("Data Source=FAME.hall;Version=3;");
            conexion.Open();
            string consulta = @"CREATE TABLE puntuaciones (nombre TEXT, puntuacion INTEGER, avatar TEXT)";
            SQLiteCommand comando = new SQLiteCommand(consulta, conexion);
            comando.ExecuteNonQuery();
            conexion.Close();
        }

        /// <summary>
        /// Lee los datos desde la base de datos y los agrega a la lista
        /// </summary>
        private void LeerDatos()
        {
            conexion.Open();
            Jugador jugador;
            string consulta = @"SELECT * FROM puntuaciones";
            SQLiteCommand comando = new SQLiteCommand(consulta, conexion);
            SQLiteDataReader lector = comando.ExecuteReader();
            while(lector.Read())
            {
                jugador.nombre = lector["nombre"].ToString();
                jugador.puntuacion = int.Parse(lector["puntuacion"].ToString());
                jugador.avatar = lector["avatar"].ToString();
                datos.Add(jugador);
            }
            conexion.Close();
            while (datos.Count < 10) //Si no hay 10 jugadores, rellenamos con los predefinidos
            {
                jugador.nombre = "NULL";
                jugador.puntuacion = 000;
                jugador.avatar = "interrogante";
                datos.Add(jugador);
            }
        }

        /// <summary>
        /// Agrega un nuevo jugador a la lista y la reordena
        /// </summary>
        /// <param name="nombre"></param>
        /// <param name="avatar"></param>
        /// <param name="puntuacion"></param>
        public void AgregarRegistro(string nombre, string avatar, int puntuacion)
        {
            Jugador jugador;
            jugador.nombre = nombre;
            jugador.puntuacion = puntuacion;
            jugador.avatar = avatar;
            datos.Add(jugador);
            datos = datos.OrderByDescending(x => x.puntuacion).ToList(); //Reordenamos la lista por puntuación
        }

        /// <summary>
        /// Reescribe todos los datos en la base de datos
        /// </summary>
        public void EscribirDatos()
        {
            conexion.Open();
            string consulta = @"DELETE FROM puntuaciones";
            SQLiteCommand comando = new SQLiteCommand(consulta, conexion);
            comando.ExecuteNonQuery();
            for (int i=0;i<10;i++)
            {
                string nombre = string.Format("'{0}',", datos[i].nombre);
                string puntuacion = string.Format("'{0}',", datos[i].puntuacion);
                string avatar = string.Format("'{0}'", datos[i].avatar);
                consulta = string.Concat(@"INSERT INTO puntuaciones VALUES (",nombre,puntuacion,avatar,")");
                comando = new SQLiteCommand(consulta, conexion);
                comando.ExecuteNonQuery();
            }
            conexion.Close();
        }
        #endregion

        #region Propiedades
        /// <summary>
        /// Devuelve la puntuación más baja
        /// </summary>
        public int PuntuacionMasBaja
        {
            get
            {
                return datos[9].puntuacion; //Puntuación del jugador 10
            }
        }

        /// <summary>
        /// Devuelve la lista de puntuaciones
        /// </summary>
        public List<Jugador> Puntuaciones
        {
            get
            {
                return datos;
            }
        }
        #endregion
    }
}
