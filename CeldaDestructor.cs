﻿namespace Problema1
{
    class CeldaDestructor:Celda
    {
        #region Constructor
        public CeldaDestructor()
        {

        }

        public CeldaDestructor(int fila, int columna, int id) : base(fila, columna, id)
        {

        }
        #endregion

        #region Metodos
        public override string Mostrar(Form1 formulario, int jugador)
        {
            Tablero tablero;
            if (jugador == 1)
            {
                tablero = Program.juego.TableroJuego;
            }
            else
            {
                tablero = Program.juego2.TableroJuego;
            }
            string tipo = GetType().Name;
            if (tablero.BuscarCeldaID(this))
            {
                estado = Estados.Tocado;
            }
            else
            {
                estado = Estados.Hundido;
                formulario.ActualizarCantidadBarcos(tipo);
            }
            return Estado;
        }
        #endregion
    }
}
