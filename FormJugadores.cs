﻿using System;
using System.Windows.Forms;

namespace Problema1
{
    public partial class FormJugadores : Form
    {
        public FormJugadores()
        {
            InitializeComponent();
        }

        private void Accionar(object sender, MouseEventArgs e)
        {
            PictureBox imagen = (PictureBox)sender;
            switch (imagen.Name)
            {
                case "bJug1":
                    Program.jugadores = 1;
                    break;
                case "bJug2":
                    Program.jugadores = 2;
                    break;
            }
            Close();
        }

        private void Seleccionado(object sender, EventArgs e)
        {
            PictureBox imagen = (PictureBox)sender;
            imagen.Cursor = Cursors.Hand;
            if (imagen.Name=="bJug1")
            {
                imagen.Image = Properties.Resources._1jugadorON;
            }
            else
            {
                imagen.Image = Properties.Resources._2jugadoresON;
            }
        }

        private void Deseleccionado(object sender, EventArgs e)
        {
            PictureBox imagen = (PictureBox)sender;
            imagen.Cursor = Cursors.Default;
            if (imagen.Name == "bJug1")
            {
                imagen.Image = Properties.Resources._1jugadorOFF;
            }
            else
            {
                imagen.Image = Properties.Resources._2jugadoresOFF;
            }
        }
    }
}
