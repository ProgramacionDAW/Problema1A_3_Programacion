﻿namespace Problema1
{
    class CeldaSubmarino:Celda
    {
        #region Constructor
        public CeldaSubmarino()
        {

        }

        public CeldaSubmarino(int fila, int columna):base(fila,columna)
        {

        }
        #endregion

        #region Metodos
        public override string Mostrar(Form1 formulario, int jugador)
        {
            estado = Estados.Hundido;
            string tipo = GetType().Name;
            formulario.ActualizarCantidadBarcos(tipo);
            return Estado;
        }
        #endregion
    }
}
